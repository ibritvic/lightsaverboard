# LightSaverBoard
UDOO board logic for light saver project

Server(Python on Linux) and Gateway(Arduino)

## Install on UDOO
### Node install
> Download ARM V7 version
```sh
wget https://nodejs.org/dist/v4.0.0/node-v4.0.0-linux-armv6l.tar.gz 
tar -xvf node-v4.2.2-linux-armv6l.tar.gz 
cd node-v4.2.2-linux-armv6l
```
> Instal - just copy
```sh
sudo cp -R * /usr/local/
```
To install serial port, node-pre-gyp must work, try with build-essential package and check versions:

* GCC and G++ 4.8 or newer, or
* CLANG and CLANG++ 3.4 or newer
* Python 2.6 or 2.7
* GNU Make 3.81 or newer

### Startup script
>Run central logic
```sh
$ cd /homeProjectDir/Server
$ node server.js
```
