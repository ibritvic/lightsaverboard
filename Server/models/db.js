"use strict";
var loki = require("lokijs");
const ENV = process.env.NODE_ENV || 'development',
      defaultConfig = require("../config");
var util = require('../util');
var handleError = util.handleError, projectLoki = util.projectLoki;
var Sensor = require('./sensor');
var Room = require('./room');

let db;
let sensors, switches, rooms, config;

//no projection so now Mongo style projection
//db.collection.prototype
/** load database
 * if exists load from disk if not create new collection
 * persisted manually only configuration not sensor data (in memory until sent to cloud)
 */
exports.loadDatabase = (callback) => {
    db = new loki(defaultConfig[ENV].database_name, {
        autoload: true,
        autoloadCallback : loadHandler,
        autosave: true,
        autosaveInterval: 10000
    });
    //Error event
    db.on("error", handleError);

    function loadHandler(){ //closure, bug
        //Check if collections exists
        sensors = db.getCollection('sensors');
        if (!sensors) {
            sensors = db.addCollection("sensors", {
                unique: ["node"],
                autoupdate: true
            });
        }

        switches = db.getCollection('switches');
        if(!switches) {
            switches = db.addCollection("switches", {
                unique: ["name"],
                autoupdate: true
            });
        }

        rooms = db.getCollection('rooms');
        if(!rooms) {
            rooms = db.addCollection("rooms", {
                unique: ["name"],
                autoupdate: true
            });
        }

        config = db.getCollection('config');
        if(!config) {
            config = db.addCollection("config", {
                autoupdate: true
            });
            config.insert(defaultConfig);
        }
        callback();
    }
};


//return switches and sensors
exports.getSensors = () => sensors;

exports.getSwitches = () => switches;

/**
 * Group switches by rooms so U can make actions upon sensor readings
 */
exports.getSwitchesByRoom = () => {
    let roomSwitches = {};
    for (let room of rooms.data) {
        roomSwitches[room.$loki] = [];
    }

    for (let sw of switches.data){
        roomSwitches[sw.room].push(sw);
    }

    return roomSwitches;
};

/**
 * find switch by id
 * @param id
 * @returns {*}
 */
exports.getSwitch = (id) => {
    if (!db || !switches)
        return handleError("No database or switches collection;");

    return switches.get(id);
};

exports.getConfig = () => {
    if (!db || !config)
        return handleError("No db or no config in database!");

    //get config data from env
    return config.data[0][ENV];
};

exports.saveConfig = () => {
    //TODO: implement
    throw new Error('No code here')
};

/**
 * prepare data for upload
 * pack sensors and switches in room
 * @returns {Array} of rooms
 */
exports.getJSONData = () => {
    if (!db)
        return handleError("No database to POST;");
    var data = [];

    //shadow vars, project in plain js array and get rid of $loki
    let pRooms    = projectLoki(rooms.data,   ["$loki",'name']);
    let pSensors  = projectLoki(sensors.data, ["$loki",'name', 'values']);
    let pSwitches = projectLoki(switches.data,["$loki", 'name', 'actions']);

    for (let room of pRooms){
        room.sensors = [];
        room.switches = [];
        for (let sw of pSwitches){
            if(sw.room == room.$loki) room.switches.push(sw);
        }
        for (let sensor of pSensors){
            if(sensor.room == room.$loki) room.sensors.push(sensor);
        }
        data.push(room);
    }
    return data;
    /*
    var room = {name: 'Room1'};
    room.sensors = projectLoki(sensors.data,["$loki",'name', 'values']);
    room.switches = projectLoki(switches.data, ["$loki", 'name', 'actions']);
    return [room]; //No need for stringify
    */
};

/**
 * Receive config data of sensor_switch matrix and save it to Loki
 * @param JSONTxt - serializedDB
 */
exports.saveMatrix = (JSONTxt) => {
    if (!db) //db must be existant or abort
       return handleError("Save matrix: no database to save into!");

    db.loadJSON(JSONTxt);
    db.save();
    //Maybe better way to refresh
    switches = db.getCollection('switches');
    sensors = db.getCollection('sensors');
};


/**
 * get config Matrix as JSON
 * @returns {{sensors: Array, switches: Array}}
 */
exports.getMatrix = () => {
    if (!db)
        return handleError("No database to POST;");
    else
        clearData(); //send just config data

    return db.serialize();  //No need for JSON Stringify
};

/**
 * Clear all sensor and switch values after post to server
 */
exports.clearData = clearData;

function clearData(){
    if (db){
        for(let sensor of sensors.data)
            sensor.values=[];

        for(let sw of switches.data)
            sw.actions = [];
    }
};
/**
 * Create new sensor, do nothing if it is already in db
 * @param name
 */
exports.addSensor = (name, type, node) => {
    var sensor = sensors.by('node', node);
    if (!sensor){
        sensor = new Sensor(name, type, node);
        sensors.insert(sensor);
    }
};

/**
 * Update new received value in sensor, autoupdate on collection!
 * @param node
 * @param value
 */
exports.updateSensor = (node, type, value) => {
    if (!db)
        return handleError("No database!");

    var sensor = sensors.by('node', node);
    if (!sensor)
        return handleError('No sensor on the node: ' + node);

    sensor.values.push({type:type, value:value, time: new Date()});
    return sensor;
};

/* no need, update directly
 * Update switching, autopdate expected
 * @param name
 * @param action

exports.updateSwitch = (name, action) => {
    var sw = switches.by('name', name);
    sw.actions.push(action);
};
*/




