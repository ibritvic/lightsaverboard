"use strict";
/**
 * Class Switch
 * letter, number - eg. A, 1 - way they are configured, values - on/off actions
 */
var Switch = class {
    constructor(name, type, letter, number){
        this.name = name;
        this.type= type;
        this.letter = letter; //For intertechno, other switches may not need it, leave it blanc
        this.number = number;
        this.actions = [];
        this.room = null; //no room until config
    }
};

module.exports = Switch;