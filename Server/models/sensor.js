"use strict";
/**
 * Sensor class
 * node - nr. of node that is represented in network
 * values, sensor readings
 * switches - list of switches that it affects
 */
var Sensor = class {
    constructor(name, type, node){
        this.name = name;
        this.type = type;
        this.node = node;
        this.values = []; //Values like {'T': 31} should be time here?
        this.room = null; //no room until config
    }
};

module.exports = Sensor;