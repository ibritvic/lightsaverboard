const winston = require("winston");
const config = require("../config")[process.env.NODE_ENV || 'development'];

const log_file =config.log_file || 'logs/log.log';
const error_log_file =  config.error_log_file || 'logs/error.log';



var logger = new winston.Logger({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: log_file, maxsize:1000000, maxFiles:5 })
    ],
    exceptionHandlers: [
        new (winston.transports.Console)(),
        new winston.transports.File({ filename: error_log_file })
    ]
});
logger.emitErrs = false; //dont throw exceptions



logger.registerBroker = ()=>{
    var broker = require('../gateway/broker'); //Circular?
    logger.on('logging', function (transport, level, msg, meta) {
        if(transport.name == 'console')
            broker.publish("server/log", level + ":" + msg);
    });
};

module.exports = logger;