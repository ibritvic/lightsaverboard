"use strict";
var logger = require('./logger');

exports.handleError = (error) => {
    logger.error(error);
    return null;
};

/**
 * Create result array with fields listed in projection
 * @param lokiArray
 * @param projection
 * @returns {Array}
 */
exports.projectLoki = (lokiArray, projection) => {
    var resultArray = [];  //value copy with slice is not good idea because it
    for(let obj of lokiArray){
        var resultObj = {};
        for (let property of projection){
            if (property in obj){
                var resultProperty = property!='$loki' ? property : 'id'; //send $loki as id , possible probs with $
                resultObj[resultProperty] = obj[property];
            }
        }
        resultArray.push(resultObj);
    }
    return resultArray;
};