var db = require("./models/db");
var ENV = process.env.NODE_ENV ||'development';
var config = require("./config")[ENV];


//Init logger
require('./util/logger');


db.loadDatabase(() => { //Start when DB is initialized
    //require('./config/sensor_switch');  //Test sensor switch config
    // require("./broker"); //gateway out, MQTT broker in
    require('./gateway');

    if (config.upload){
        const EventEmitter = require("events").EventEmitter,
            upload = require("./cloud").post,
            ee = new EventEmitter();

        upload();

        ee.on("uploadData", function () {
            upload();
        });

        setInterval(() => ee.emit("uploadData"), config.report_period);
    }
});
