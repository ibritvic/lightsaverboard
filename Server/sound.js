var Sound = require('node-aplay');

var music = new Sound('./leni.wav');

// you can also listen for various callbacks:
music.on('complete', function () {
    console.log('Done with sound play!');
});

exports.play = () => {
    music.play();
};
