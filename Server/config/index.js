module.exports = {
    development: {
        upload: true,
        port: 3003,
        mqtt_port: 1883,
        ws_mqtt_port: 1884,
        database_name: 'lights.json',
        log_file: 'logs/log.log',
        error_log_file:'logs/error.log',
        app: {
            name: 'LightSaver Board',
            author: 'ibritvic'
        },
        report_period: 240000,
        sender: 'IT', //Intertechno
        receiver: 'ZW', //Serial
        serial_port: '/dev/ttymxc3',
        baudrate: 19200,
        light_level: 300,
        work_hours: {start:9,end:22},
        motion_wait_work: 15000, // milliseconds
        motion_wait_break: 6000, // milliseconds
        cloud_url: 'http://192.168.5.13:3009/board/data',//'http://light.iot2.eu/board/data','http://192.168.5.13:3009/board/data',
        switch_ip: '192.168.5.17',
        switch_port: 49880,
        zwave_device: '/dev/ttyACM0'
    },
    test: {
        port: 3003,
        mqtt_port: 1883,
        database_name: 'lights.json',
        app: {
            name: 'LightSaver Board',
            author: 'ibritvic'
        },
        serial_port: '/dev/tty-usbserial1',
        baudrate: 19200,
        light_level: 512,
        cloud_url: 'http://192.168.5.13:3000/board/data'
    },
    production: {
        upload: true,
        port: 80,
        secret: 'dsQwe3Asd6fSavto8PolASfvfh86654fw'
    }
};
