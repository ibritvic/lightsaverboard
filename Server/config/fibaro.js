/**
 * Fibaro settings
**/

module.exports = {
    'FGMS01' : { //motion sensor settings index:value according to https://github.com/OpenZWave/open-zwave/blob/master/config/fibaro/fgms.xml
        2  : 5, //3 secs PIR blindness
        40 : 100, //Illumination report threshold lux (default 200)
        42 : 240, //Illumination report interval, 2 mins
        60 : 0, //Temperature report threshold: dont sen'd reports ontemp change, send them regullary
        64 : 1800 ////Temperature report interval secs
    }
};