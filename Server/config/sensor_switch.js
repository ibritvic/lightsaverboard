"use strict";
var Switch = require('../models/switch');
var Sensor = require('../models/sensor');

exports.getJSON = () => {
    var switch1 = (new Switch ('A1','A', 1));
    var switch2 = (new Switch ('A2','A', 2));

    var sensor1 = new Sensor('L1', 'L');
    var sensor2 = new Sensor('O1', 'O');

    sensor1.switches = [switch1.name, switch2.name]; //Which switches it influences
    sensor2.switches = [switch1.name, switch2.name];

    var obj = {
        'switches' : [switch1, switch2],
        'sensors': [sensor1, sensor2]
    };

    return JSON.stringify(obj);
};



var switch1 = (new Switch ('A1','A', 1)).save();
var switch2 = (new Switch ('A2','A', 2)).save();

var sensor1 = new Sensor('L1', 'L');
var sensor2 = new Sensor('O1', 'O');

/*
sensor1.switches = [switch1, switch2]; //Which switches it influences
sensor2.switches = [switch1, switch2];
*/
//sensor1.save();
// sensor2.save();
