var ENV = process.env.NODE_ENV ||'development';
var config = require("../config")[ENV];
var com = require("serialport");
var handleError = require("../util").handleError;

/**
 * start monitoring and report sensor name/value when received
 * @param cb
 */
exports.start = (cb) => {
    "use strict";
    var serialPort = new com.SerialPort(config.serial_port, {
        baudrate: config.baudrate,
        parser: com.parsers.readline('\r\n')
    }, true); //open immediately flag

    serialPort.on('error',(error) => {handleError(error)});

    serialPort.open( (error) => {
        if (error)
            return handleError("Can't open serial port! ");

        serialPort.on('data', (data) => {
            console.log('data received: ' + data);
            var sensorType = data.charAt(0); //First letter says which character we have
            var sensorValueString = data.substr(1); //Rest of the data should be number
            var sensorValue = parseFloat(sensorValueString);

            if(sensorValue != sensorValueString)// trick to check if it is number
                return handleError('Sensor value not number: ' + sensorValueString);

            var sensorType = sensorType=='M'?'O':sensorType; //Change Motion to Occupancy
            var sensorName =  sensorType == 'L' ? 'L1' : 'O1'; //Only one sensor from serial
            cb(sensorName, sensorType, sensorValue);
        });
    });
};

