/**
 * MQTT Broker for communication with OpenHAB
 * Receive sensor readings and send commands
 **/
"use strict";

var mosca = require('mosca');
var log = require('../util/logger');
var db = require('../models/db');
const config = db.getConfig();
const ENV = process.env.NODE_ENV ||'development';

var nrClients = 0;
var server;

exports.start = (receiveCB, sendCB) => {
    //Start a new mqtt broker server, no persistance
    server = new mosca.Server({
        port:config.mqtt_port,
        http: {  //for websockets
            port: config.ws_mqtt_port,
            bundle: true,
            static: './'
        }
    });

    server.on('clientConnected', (client) => {
        log.info('client connected: ' + client.id);
        nrClients++;
    });

// fired when a client disconnects
    server.on('clientDisconnected', (client)=> {
        log.info('Client Disconnected:'+ client.id);
        nrClients--;
    });

// fired when a message is received
    server.on('published', (packet, client) => {
        // do not use logger - infinite loop console.log('Published packet:'+ JSON.stringify(packet));
        route(packet, client);
    });

    server.on('ready', () => {
        log.registerBroker(); //start MQTT emit of logs
        log.info('Mosca MQTT broker running on ports: ' + config.mqtt_port + ', ' + config.ws_mqtt_port );
    });
};

//circular reference, should work though
const executeCommand = require('./index').executeCommand;
let route = (packet, client)=>{

    var topic = packet.topic.trim();
    switch(topic){
        case 'client/connect':
            //Send an environment on connect
            publish('server/env', ENV);
            //Send a copy of db on connected
            publish('server/config', db.getMatrix());
            break;
        case 'client/config':
            db.saveMatrix(packet.payload);
            break;
        case 'client/command':
            executeCommand(JSON.parse(packet.payload));
            break;
    }
};


var publish = (topic, payload)=> {
    if (server && nrClients>0) {
        let packet = {
            'topic': topic,
            'payload': payload
        };
        server.publish(packet);
    }
};

exports.publish = publish;  //Publish from other modules like logging

exports.test = ()=> {
    if (nrClients < 1){
        log.info('No clients to test! ');
        return;
    };
    var command = 'ON';
    let testSwitch = ()=> {

        var firstPacket = {
            //toggle on of switch
            topic: '/in/Light1_Ivan/command', //Lighter in
            payload: command
        };
        command = command=='ON'?'OFF':'ON';
        var secondPacket = {
            //toggle on of switch
            topic: '/in/Light_GF_Corridor_Ceiling/command', //lighter in
            payload: command
        };
        server.publish(firstPacket);
        server.publish(secondPacket);
        /*
        udp.send(intertechno.getCommand('A',1,command=='ON'));
        udp.send(intertechno.getCommand('A',2,command!='ON'));
        */

    };
    setInterval(testSwitch, 5000);
};


