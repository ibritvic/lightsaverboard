/**
 * ZWave library for sensors
 * REad
 * TODO: Find patch for lost inclusion after server restart
 * @type {*|string}
 */
"use strict";
const ENV = process.env.NODE_ENV ||'development';
const config = require("../config")[ENV];
const OZW = require('openzwave-shared');
const fibaro = require('../config/fibaro')['FGMS01'];
//const sound = require('../sound');
var logger = require('../util/logger');
var db = require("../models/db");



//sudo ldconfig /usr/local/lib64 (for amd64 Ubuntu)if it crashes after require

var nodes = [];

var zwave = new OZW({
    Logging: false,     // disable file logging (OZWLog.txt)
    ConsoleOutput: true // enable console logging
});

exports.start = (callback)=> {
    zwave.on('driver ready', function (homeid) {
        logger.info('scanning homeid=0x%s...', homeid.toString(16));
    });

    zwave.on('driver failed', function () {
        logger.error(config.zwave_device + ' - failed to start driver');
        zwave.disconnect();
    });

    zwave.on('node added', function (nodeid) {
        nodes[nodeid] = {
            manufacturer: '',
            manufacturerid: '',
            product: '',
            producttype: '',
            productid: '',
            type: '',
            name: '',
            loc: '',
            classes: {},
            ready: false,
        };
    });

    /**
     * On value added change to selected configuration
     */
    zwave.on('value added', function (nodeid, comclass, value) {
        if (!nodes[nodeid]['classes'][comclass])
            nodes[nodeid]['classes'][comclass] = {};

        //Check if it is in config change of default config value
        //TODO: jus looking if is it class 112 and index in config, should check if it is fibaro, but don't have that info here???
        if (comclass == 112){
            if (fibaro.hasOwnProperty(value.index)){
                if (value.value != fibaro[value.index]){
                    value.value = fibaro[value.index]; //change value to one from config file
                    zwave.setValue(value, fibaro[value.index]); //write to sensor
                }
            }
        }
        nodes[nodeid]['classes'][comclass][value.index] = value;
    });

    zwave.on('value changed', function (nodeid, comclass, value) {
        //  if (nodes[nodeid]['ready']) { //Ready meaning only on start
        logger.info('node%d: changed: %d:%s:%s->%s %s', nodeid, comclass,
            value['label'],
            nodes[nodeid]['classes'][comclass][value.index]['value'],
            value['value'], new Date().toLocaleTimeString());
        //  }
        nodes[nodeid]['classes'][comclass][value.index] = value;
        /*Leni tease
        if(comclass==48 &&  value['value'])
            sound.play(); */

        if(comclass==48){
            callback(nodeid, 'O', value['value']?1:0 );
        }else if(comclass==49 && value['label'].indexOf('uminan')>0){ //Luminance
            callback(nodeid, 'L', value['value'] );
        }else if(comclass==49 && value['label'].indexOf('emper')>0){ //Temperature
            callback(nodeid, 'T', value['value']);
        }
    });

    zwave.on('value refreshed', function(nodeid, commandclass, valueId){
        logger.info('node%d: refreshed: %d:%s:%s->%s', nodeid, comclass,
            value['label'],
            nodes[nodeid]['classes'][comclass][value.index]['value'],
            value['value']);

    });

    zwave.on('value removed', function (nodeid, comclass, index) {
        if (nodes[nodeid]['classes'][comclass] &&
            nodes[nodeid]['classes'][comclass][index])
            delete nodes[nodeid]['classes'][comclass][index];
    });
    /* no need, node ready will show something
     zwave.on('node naming', function(nodeid, nodeinfo){
     logger.log('node%d naming:' +JSON.stringify(nodeinfo),nodeid);
     });
     */

    zwave.on('node ready', function (nodeid, nodeinfo) {
        nodes[nodeid]['manufacturer'] = nodeinfo.manufacturer;
        nodes[nodeid]['manufacturerid'] = nodeinfo.manufacturerid;
        nodes[nodeid]['product'] = nodeinfo.product;
        nodes[nodeid]['producttype'] = nodeinfo.producttype;
        nodes[nodeid]['productid'] = nodeinfo.productid;
        nodes[nodeid]['type'] = nodeinfo.type;
        nodes[nodeid]['name'] = nodeinfo.name;
        nodes[nodeid]['loc'] = nodeinfo.loc;
        nodes[nodeid]['ready'] = true;
        logger.info('node%d: %s, %s', nodeid,
            nodeinfo.manufacturer ? nodeinfo.manufacturer
                : 'id=' + nodeinfo.manufacturerid,
            nodeinfo.product ? nodeinfo.product
                : 'product=' + nodeinfo.productid +
            ', type=' + nodeinfo.producttype);
        logger.info('node%d: name="%s", type="%s", location="%s"', nodeid,
            nodeinfo.name,
            nodeinfo.type,
            nodeinfo.loc);

        /*On nmode ready save the sensor in the db
            let myName = nodeinfo.manufacturer.slice(3) + nodeinfo.manufacturerid;
            db.addSensor(myName, nodeinfo.type);
        */

        for (let comclass in nodes[nodeid]['classes']) {
            switch (parseInt(comclass)) {
                case 0x25: // COMMAND_CLASS_SWITCH_BINARY
                case 0x26: // COMMAND_CLASS_SWITCH_MULTILEVEL
                    zwave.enablePoll(nodeid, comclass);
                    break;
                case 0x30: // 48 COMMAND_CLASS_SENSOR_BINARY
                case 0x31: // COMMAND_CLASS_SENSOR_MULTILEVEL
                case 0x32: //COMMAND_CLASS_METER
                    let myName = nodeinfo.manufacturer.substr(0,3) + nodeinfo.productid;
                    db.addSensor(myName, nodeinfo.type, nodeid); //save it to db if it is not there already
                    break;
            }
            var values = nodes[nodeid]['classes'][comclass];
            logger.info('node%d: class %d', nodeid, comclass);
            for (let idx in values)
                logger.info('node%d:   %s=%s', nodeid, values[idx]['label'], values[idx]['value']);
        }
    });

    zwave.on('notification', function (nodeid, notif) {
        switch (notif) {
            case 0:
                logger.info('node%d: message complete',nodeid);
                break;
            case 1:
                logger.info('node%d: timeout' , nodeid);
                break;
            case 2:
                logger.info('node%d: nop', nodeid);
                break;
            case 3:
                logger.info('node%d: node awake', nodeid);
                break;
            case 4:
                logger.info('node%d: node sleep',  nodeid);
                break;
            case 5:
                logger.info('node%d: node dead',  nodeid);
                break;
            case 6:
                logger.info('node%d: node alive',  nodeid);
                break;
        }
    });

    zwave.on('scan complete', function () {
        logger.info('====> scan complete.');
        // set dimmer node 5 to 50%
        //zwave.setValue(5,38,1,0,50);
        //zwave.setValue( {node_id:5, class_id: 38, instance:1, index:0}, 50);
        // Add a new device to the ZWave controller using new security API v>=1.3
        // set this to 'true' for secure devices eg. door locks
        //zwave.addNode(false);
        // zwave.requestConfigParam(3, 40);
    });


    zwave.on('controller command', function (r, s) {
        console.log('controller commmand feedback: r=%d, s=%d', r, s);
    });

    zwave.connect(config.zwave_device);  // connect to a USB ZWave controller

    //kill zwave connection when app is closing
    var cleanup = () => {
        logger.info("ZWave disconnecting...")
        zwave.disconnect(config.zwave_device);
        process.exit(2);
    };

    /* Disable this
    zwave.on('node event', function(nodeid, nodeEvt) {
        logger.info(new Date(), 'node event', nodeid, nodeEvt);
    });
    */


  // process.on('exit', cleanup);
   process.on('SIGINT', cleanup);
};

//Needed for inclusion, securtiy prob?
exports.zwave = zwave;

