"use strict";
var algorithm = require("../algorithm");
var db = require("../models/db");
const config = db.getConfig();

var handleError = require("../util").handleError;
var receiver, sender;

//get switches grouped in rooms
var switches = db.getSwitchesByRoom();

switch (config.receiver) {
    case 'SE':
        receiver = require('./serial');
        break;
    case 'ZW':
        receiver = require('./zwave');
        break;
    default:
        receiver = require('./serial');
}


switch (config.sender) {
    case 'SE':
        sender = require('./intertechno');
        break;
    default:
        sender = require('./intertechno');
};


/**
 *
 * @param sensorValueType - 'L' and 'O' - light level, occupancy
 * @param sensorValue -
 */
let receiveCB = (node, sensorValueType, sensorValue) => {
    switch(sensorValueType){
        case 'O':
            calculateAction(node, sensorValueType, sensorValue);
            break;
        case 'L':
            calculateAction(node, sensorValueType, sensorValue);
            break;
        case 'T':
            db.updateSensor(node, sensorValueType, sensorValue );
            break;
        default:
            handleError('Uknown sensor value type: ' + sensorValueType);
    }
};

/**
 * Ask algorithm what to do then if needed change state of all connected sensor switches
 * @param node
 * @param sensorValueType
 * @param sensorValue
 */
let calculateAction = (node, sensorValueType, sensorValue) => {
    algorithm.calculate(node, sensorValueType, sensorValue, (action, error) => {
        if (error) return handleError(error);

        var sensor = db.updateSensor(node, sensorValueType, sensorValue );
        if ( action !== null && sensor && sensor.room && switches[sensor.room] ){ //no room or no switches no action
            //Find array of switches that are in the same room with this sensor, same action to all of them?
            for(let id of switches[sensor.room]){
                toggleSwitch(id, action); //in switches array there is a $loki value
            }
        }
    });
};

function toggleSwitch(id, action){
    var sw = db.getSwitch(id);
    if(sw){
        sender.send(sw, action, (error) => {
            if (!error) sw.actions.push({ action: action, time: new Date()}); //counting on autoupdate
        });
    }
} ;



/**
 * Execute received command from switch
 * @param command={name, deviceId, value}
 */
exports.executeCommand = (command) => {
    if (command.name == 'switch'){
        toggleSwitch(command.deviceId, command.value);
    }
};

//start receiving
receiver.start(receiveCB);

//start broker
require("./broker").start();