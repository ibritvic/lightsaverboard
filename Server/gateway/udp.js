"use strict";
var db = require("../models/db");
const config = db.getConfig();
let dgram = require('dgram');
let server = dgram.createSocket('udp4');
let port = config.switch_port; //Intertechno
var host = config.switch_ip;
var log = require('../util/logger');

server.on('error', (err) => {
    log.error('server error:\n' + err.stack);
    server.close();
});

server.on('message', (msg, rinfo) => {
    //log.info("server got: " +  msg + "from " + rinfo.address + ":" + rinfo.port);
});

server.on('listening', () => {
    var address = server.address();
    log.info('server listening' + address.address + ':' + address.port);
});

server.bind(port);
// server listening 0.0.0.0:port

exports.send = (message) => server.send(message, 0, message.length, port, host, (err)=> { //change to localhost for testing
    if(err) log.error('UDP error: ' + err.message);
});
