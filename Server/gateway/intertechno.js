/**
 * Intertechno logic for switching
 */

"use strict";
let WheelHigh = "1,3,3,1,";
let WheelLow = "1,3,1,3,";
let WheelPreSync = "1,3,1,3,1,3,3,1,1,3,3,1,";
let WheelSync = "1,31,";
let WheelEnd = "0";
let WheelHeader = "0,0,6,0,360,26,0,";
var udp = require('./udp');
var log = require('../util/logger');

function encodeBits(lh, val, nbits) {
    var encoded = ""; // io.realm.internal.Table.STRING_DEFAULT_VALUE;
    var shift = nbits - 1;
    for (var i = 0; i < nbits; i++) {
        encoded = encoded + lh[(val >> (shift - i)) & 1];
    }
    return encoded;
}

function inverse4Bits(i) {
    return ((((i & 1) << 3) | ((i & 2) << 1)) | ((i & 4) >> 1)) | ((i >> 3) & 1);
}
/**
 * Create code that will switch Intertechno device through gateway
 * @param letter - letter in mobile app with which is paired
 * @param number - number mobile app
 * @param state - Boolean - On/Offd
 * @returns {string}
 */
function commandWheel(letter, number, state) {
    var lh = [WheelLow, WheelHigh];
    var c = inverse4Bits(letter);
    var btn = encodeBits(lh, (c << 4) + inverse4Bits(number - 1), 8);
    /* state should be already be 0/1
    if (!state)  i = 0;
    */
    return WheelHeader + btn + WheelPreSync + lh[state] + WheelSync + WheelEnd;
}
/**
 * Get switch and action
 * @param sw - Switch type
 * @param action - true/false - on/off
 * @param cb
 */
exports.send = (sw, action, cb) => {
    log.info("UDP IT switch: " + sw.letter + sw.number + ' ' + action );
    udp.send(commandWheel(sw.letter, sw.number, action));
    cb();
}
