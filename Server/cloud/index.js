var request = require('request');
var config = require('../config')[process.env.NODE_ENV ||'development'];
var handleError = require('../util').handleError;
var db = require('../models/db');
/*var mqtt =require('mqtt');

const client = mqtt.connect(config.cloud_url);
var connected = false;

client.on('connect', function () {
    connected = true;
    client.subscribe('command');
});

client.on('message', function (topic, message) {
    // message is Buffer
    if(topic.indexOf('command')!= -1){
        console.log('command: ' + message.toString());
    }
});

exports.sendData = (data) => {
    if (!connected)
        return handleError('Client not connected');

    client.publish('data',db.getJSONData());
    db.clearData(); //TODO:Check if publish succeeded
};
*/


exports.post = () => {

    request
        .post(config.cloud_url)
        .json(db.getJSONData())
        .on('response', function (response) {
            console.log("Cloud response SC: " + response.statusCode); //DEBUG ONLY
            if (response.statusCode == 200) db.clearData(); //clear DB on board after succesfull POST
        })
        .on('error', handleError);
};
