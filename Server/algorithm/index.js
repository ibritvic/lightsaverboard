"use strict";
var db = require("../models/db");
const config = db.getConfig();
var matrix = require('./matrix');


var light = {}; //Light On/Off presume Off
var occupation = {}; //Presence in room , start 0

//Turn off light differently on work hours (wait more for off)
var waitWork = config.motion_wait_work,
    waitBreak = config.motion_wait_break,
    workTimeStart = config.work_hours.start,
    workTimeEnd = config.work_hours.end;

/**
 * Act on sensor reading, check algorithm and switch on/off
 * @param node
 * @param sensorValueType
 * @param sensorValue
 * @param callback
 */
var calculateAction = (node, sensorValueType, sensorValue, callback) => {
    var wait = 0;
    //Remember values and trigger action
    if(sensorValueType == 'L'){//Light sensor
        light[node] = sensorValue < config.light_level ? 0 : 1; //Enough light in  a room?
    } else if(sensorValueType == 'O'){ //Occupancy
        occupation[node] = sensorValue==0 ? 0 : 1; //convert to int;
    }

    //Delay could be in matrix to, now is simple se it can stay also
    if (occupation[node] == 0) {  //occupation 0 wait for some time before action
        let hour = (new Date()).getHours();
        wait = (hour >= workTimeStart && hour <= workTimeEnd) ? waitWork : waitBreak; //different wait for working/non hours
        setTimeout(()=>{
            callback(decide(light[node], occupation[node]));
        }, wait);
    }else{
        callback(decide(light[node], occupation[node])); //someone in room act immediately
    }
};

var decide = (l,o)=>{
    let roomLight = l!==undefined? l : 0; //If not set presume dark
    let roomOccupation = o!==undefined? o : 0; //If not set presume empty
    return matrix[roomLight][roomOccupation];
};

exports.calculate = calculateAction;