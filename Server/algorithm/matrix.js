/**
 * Return matrix of decisions
 * [light][occupancy] = action - 0 - off, 1 - On, -1 No action
 * @type {}
 */

var decisionMatrix = {
    0: {   // 0 as light
        0: null, // 0 as Occupancy, null as do nothing
        1: 1
    },
    1: {
        0: 0,
        1 : null
    }
};


module.exports = decisionMatrix;