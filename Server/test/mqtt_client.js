var mqtt =require('mqtt');
var  host= 'localhost';
var client = mqtt.connect('ws://'+ host + ':1883');
var connected = false;

client.on('connect', function () {
    connected = true;
    client.subscribe('command');
    console.log('connected');
});

client.on('message', function (topic, message) {
    // message is Buffer
    if(topic.indexOf('command')!= -1){
        console.log('command: ' + message.toString());
    }
});

module.exports = client;