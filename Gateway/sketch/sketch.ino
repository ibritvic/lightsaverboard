/**
*  Gateway UDOO board sketch
*  by: ibritvic
*
*/


#define RELAY_PIN  23  //Relay pin - digital out
#define PIR_PIN 22 //PIR Motion sensor - digital in
#define RCVSIZE 2

int calibrationTime = 20;  //ŠPIR calibration from 10-60s

//the time when the PIR sensor outputs a low impulse
long unsigned int lowIn;         

//the amount of milliseconds the sensor has to be low 
//before we assume all motion has stopped
long unsigned int pause = 3000;  

boolean lockLow = true;
boolean takeLowTime;

// the setup routine runs once when you press reset:
void setup() {
  int sensorValue = 0;
  
  // initialize serial port at a baud rate of 115200 bps
  Serial.begin(19200);

  // initialize PINs
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(PIR_PIN, INPUT);

  /* calibrate light sensor during the first five seconds 
     int sensorMin = 1023;        // minimum light sensor value
    int sensorMax = 0;           // maximum light sensor value
   while (millis() < 5000) {
     sensorValue = analogRead(A0);
  
     // record the maximum sensor value
     if (sensorValue > sensorMax) {
       sensorMax = sensorValue;
     }
  
     // record the minimum sensor value
     if (sensorValue < sensorMin) {
       sensorMin = sensorValue;
     }
   }
   */
  
  //give the PIR sensor some time to calibrate
  Serial.print("Calibrating sensor ");
  for(int i = 0; i < calibrationTime; i++){
    Serial.print(".");
    delay(1000);
  }
  delay(50);
}


int counter;
void loop() {
  boolean received = false;   // store if Arduino received something
  //recieive two chars
  int msg[RCVSIZE]; //Two char codes
  int counter = 0;

  int pirSensorvalue = digitalRead(PIR_PIN);
  Serial.println("M" + String(pirSensorvalue));
  
  int lightSensorValue = analogRead(A0);
  Serial.println("L" + String(lightSensorValue));
  delay(500); //Wait for ARM
  
  while (Serial.available() > 0) {
    int in = Serial.read();
    msg[counter] = in;
    counter++;
    received = true;
  }
  
  if(received) {  
    if((char)msg[0] == 'R'){ //ASCII 82 = R as Relay1
      digitalWrite(RELAY_PIN, (char)msg[1]=='1'? 1 : 0);
      // Serial.println((char)msg[1]=='1'? 1 : 0);
    }    
   
    received = false;
  }
  
  delay(2000);
}

/*
void readLight() {
  int sensorValue = 0; //Value from photoLDR  sensor
  
  sensorValue = analogRead(A0); //read LDR
  
  // apply the calibration to the sensor reading
 // sensorValue = map(sensorValue, sensorMin, sensorMax, 0, 255);

  // in case the sensor value is outside the range seen during calibration
  //sensorValue = constrain(sensorValue, 0, 255);
  
 // int ldrVoltage = (int)(sensorValue * (5.0 / 1023.0)); //Resistance shows light level
  // send out the value you read
  Serial.println("Light level: " + String(sensorValue));
}

void readPIR(){
   if(digitalRead(PIR_PIN) == HIGH){
       if(lockLow){  
         //makes sure we wait for a transition to LOW before any further output is made:
         lockLow = false;            
         Serial.println("---");
         Serial.print("motion detected at ");
         Serial.print(millis()/1000);
         Serial.println(" sec"); 
         delay(50);
         }         
         takeLowTime = true;
      } else {
         if(takeLowTime){
          lowIn = millis();          //save the time of the transition from high to LOW
          takeLowTime = false;       //make sure this is only done at the start of a LOW phase
          }
         //if the sensor is low for more than the given pause, 
         //we assume that no more motion is going to happen
         if(!lockLow && millis() - lowIn > pause){  
             //makes sure this block of code is only executed again after 
             //a new motion sequence has been detected
             lockLow = true;                        
             Serial.print("motion ended at ");      //output
             Serial.print((millis() - pause)/1000);
             Serial.println(" sec");
             delay(50);
         }
      }      
}
*/

